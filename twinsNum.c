#include <stdio.h>
#include "primes.h"

int main(){
    int n, twins = 0;
    scanf("%d", &n);

    for(int i=1; primes[i]<=n; ++i){
        if(primes[i] - primes[i-1] == 2){
            twins++;
        }
    }

    printf("%d\n", twins);

    return 0;
}