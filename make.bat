@echo off

if exist .\primesGenerator.exe del .\primesGenerator.exe
if exist .\twinsNum.exe del .\twinsNum.exe
if exist .\primes.h del .\primes.h

if NOT "%1" == "remove" (
    gcc %~dp0primesGenerator.c -o %~dp0primesGenerator.exe
    start /wait %~dp0primesGenerator.exe
    gcc %~dp0twinsNum.c -o %~dp0twinsNum.exe
)