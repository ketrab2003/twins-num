#include <stdio.h>
#define N 1000004

char isNotPrime[N];

void eratostenes(){
    isNotPrime[0] = 1;
    isNotPrime[1] = 1;
    for(int i=2; i*i<N; ++i){
        if(!isNotPrime[i]){
            for(int j=i*i; j<N; j+=i)
                isNotPrime[j] = 1;
        }
    }
}

int main(){
    eratostenes();

    FILE *f = fopen("primes.h", "w");

    fprintf(f, "int primes[] = {2");
    for(int i=3; i<N; ++i){
        if(!isNotPrime[i])
            fprintf(f, ",%d", i);
    }
    fprintf(f, "};");

    return 0;
}