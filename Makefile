all: twinsNum.out
primesGenerator.out:
	gcc primesGenerator.c -o primesGenerator.out
primes.h: primesGenerator.out
	./primesGenerator.out
twinsNum.out: primes.h
	gcc twinsNum.c -o twinsNum.out
remove:
	rm primesGenerator.out twinsNum.out primes.h